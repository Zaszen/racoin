# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.22)
# Database: racoin
# Generation Time: 2015-02-07 04:27:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table annonces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `annonces`;

CREATE TABLE `annonces` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titre` varchar(50) DEFAULT '',
  `Description` varchar(2500) DEFAULT '',
  `Prix` int(15) DEFAULT NULL,
  `Ville` varchar(50) DEFAULT NULL,
  `CodeP` varchar(10) DEFAULT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `Password` varchar(260) DEFAULT NULL,
  `Salt` varchar(100) DEFAULT NULL,
  `DateP` date DEFAULT NULL,
  `Tel` varchar(20) DEFAULT NULL,
  `UrlImg` varchar(150) DEFAULT NULL,
  `IdDepartement` int(11) DEFAULT NULL,
  `Id_categorie` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `annonces` WRITE;
/*!40000 ALTER TABLE `annonces` DISABLE KEYS */;

INSERT INTO `annonces` (`Id`, `Titre`, `Description`, `Prix`, `Ville`, `CodeP`, `Nom`, `Email`, `Password`, `Salt`, `DateP`, `Tel`, `UrlImg`, `IdDepartement`, `Id_categorie`)
VALUES
	(1,'Peugeot 307','- Vend Peugeot 307 SW NAVTECH, 2.0 HDI 110 ch, 6 cv fiscaux\n- Anne - 18/06/2003, 194.000 km\n- Carte grise 7 place, mais 5 sièges, toit panoramique\n- 2 main, bon état, rien a prévoir\n- Options: clim automatique\n- Radio CD avec chargeur 5 CD\n- 2-vitre électriques\n- Direction assistée\n- 2 accoudoir avant\n- 4 airbags\n- Rétroviseurs électriques\n- Verrouillage centralisé des portes à distance\n- ABS, ESP, Alarme\n- Volant réglable en profondeur et hauteur banquette rabattable\n- Phares antibrouillard\n- Ordinateur de bord\n- 4 pneus hiver\n- Pompe à eau + kit de courroie de distribution changer\n- a 122000 km',15000,'paris','54000','EL HARTI','n36@hotmail.fr','81.4eckyZFNAA','8125011086','2014-12-21','0664885206','App/template/img/500.jpg',1,1),
	(2,'iPhone blanc 4s','je vend mon iPhone blanc 4s de 8go débloquer, quelques rayures à l\'arrière du téléphone sinon très bon état. ',140,'Metz','57000','EL HARTI','ay.elharti@gmail.com','81.4eckyZFNAA','8125011086','2015-01-14','0664885206','App/template/img/501.jpg',1,2),
	(35,'Toyota auris hsd 136','Tous nos véhicules sont visibles sur notre vitrine leboncoin ou sur notre site internet\r\n\r\nGARANTIE 3 MOIS Extension possible jusqu\'à 60 mois\r\n30 / 06 / 2011 - 5 cv\r\n\r\n1 ÈRE MAIN\r\n\r\nGPS TACTILE\r\nBLUETOOTH\r\nRÉGULATEUR DE VITESSE\r\nCLIM AUTOMATIQUE\r\nABS\r\nAIRBAGS FRONTAUX\r\nAIRBAGS LATÉRAUX\r\nAIRBAGS RIDEAUX\r\nAIDE AU FREINAGE D\'URGENCE\r\nAIDE AU DÉMARRAGE EN COTE\r\nSYSTÈME DE RÉCUPÉRATION D\'ÉNERGIE AU FREINAGE\r\nRÉPARTITEUR ÉLECTRONIQUE DE FREINAGE\r\nDIRECTION ASSISTÉE\r\nJANTES ALU\r\nORDINATEUR DE BORD\r\nPEINTURE MÉTAL\r\nPHARES ANTIBROUILLARD\r\nCD/MP3\r\nPRISE USB\r\nCOMMANDE AUDIO AU VOLANT\r\nRÉTROS ÉLECTRIQUES DÉGIVRANTS\r\nRÉTROS RABATTABLES ÉLECTRIQUEMENT\r\nVOLANT CUIR\r\n4 VITRES ÉLECTRIQUES\r\nDÉMARRAGE SANS CLÉ\r\nISOFIX\r\n\r\n- Tous nos véhicules sont révisés et préparés avant livraison\r\n\r\n- Nous achetons tous types de véhicule à partir de 2008, 100.000 km max\r\n\r\nGARAGE DE CHATEL\r\n15 RUE DE VERDUN\r\n57160 CHATEL ST GERMAIN\r\n0674347955\r\n0387305895\r\n\r\nDu Mardi au vendredi 9h-12h / 14h-18h30\r\nSamedi 9h-15h30 ',11990,'Châtel-Saint-Germain','57160','Ayoub El harti','n36@hotmail.fr','98ASV1DkT4Kz2','9802262167','2015-02-03','0664885206','App/template/img/large/9802262167.jpeg',1,1),
	(39,'Audi','Tous nos véhicules sont visibles sur notre vitrine leboncoin ou sur notre site internet\r\n\r\nGARANTIE 3 MOIS Extension possible jusqu\'à 60 mois\r\n30 / 06 / 2011 - 5 cv\r\n\r\n1 ÈRE MAIN\r\n\r\nGPS TACTILE\r\nBLUETOOTH\r\nRÉGULATEUR DE VITESSE\r\nCLIM AUTOMATIQUE\r\nABS\r\nAIRBAGS FRONTAUX\r\nAIRBAGS LATÉRAUX\r\nAIRBAGS RIDEAUX\r\nAIDE AU FREINAGE D\'URGENCE\r\nAIDE AU DÉMARRAGE EN COTE\r\nSYSTÈME DE RÉCUPÉRATION D\'ÉNERGIE AU FREINAGE\r\nRÉPARTITEUR ÉLECTRONIQUE DE FREINAGE\r\nDIRECTION ASSISTÉE\r\nJANTES ALU\r\nORDINATEUR DE BORD\r\nPEINTURE MÉTAL\r\nPHARES ANTIBROUILLARD\r\nCD/MP3\r\nPRISE USB\r\nCOMMANDE AUDIO AU VOLANT\r\nRÉTROS ÉLECTRIQUES DÉGIVRANTS\r\nRÉTROS RABATTABLES ÉLECTRIQUEMENT\r\nVOLANT CUIR\r\n4 VITRES ÉLECTRIQUES\r\nDÉMARRAGE SANS CLÉ\r\nISOFIX\r\n\r\n- Tous nos véhicules sont révisés et préparés avant livraison\r\n\r\n- Nous achetons tous types de véhicule à partir de 2008, 100.000 km max\r\n\r\nGARAGE DE CHATEL\r\n15 RUE DE VERDUN\r\n57160 CHATEL ST GERMAIN\r\n0674347955\r\n0387305895\r\n\r\nDu Mardi au vendredi 9h-12h / 14h-18h30\r\nSamedi 9h-15h30 ',13000,'Reims','51100','Ayoub El harti','n36@hotmail.fr','24d.tNAoyzfZY','2461222883','2015-02-03','0664885206','App/template/img/large/2461222883.jpeg',2,1),
	(40,'Toyota auris hsd 136','Toyota auris 136',13000,'Reims','51100','Ayoub El harti','n36@hotmail.fr','65J69sDP5Kr0M','6509371517','2015-02-03','0664885206','App/template/img/large/6509371517.jpeg',NULL,1),
	(41,'Toyota auris hsd 136','Tous nos véhicules sont visibles sur notre vitrine leboncoin ou sur notre site internet\r\n\r\nGARANTIE 3 MOIS Extension possible jusqu\'à 60 mois\r\n30 / 06 / 2011 - 5 cv\r\n\r\n1 ÈRE MAIN\r\n\r\nGPS TACTILE\r\nBLUETOOTH\r\nRÉGULATEUR DE VITESSE\r\nCLIM AUTOMATIQUE\r\nABS\r\nAIRBAGS FRONTAUX\r\nAIRBAGS LATÉRAUX\r\nAIRBAGS RIDEAUX\r\nAIDE AU FREINAGE D\'URGENCE\r\nAIDE AU DÉMARRAGE EN COTE\r\nSYSTÈME DE RÉCUPÉRATION D\'ÉNERGIE AU FREINAGE\r\nRÉPARTITEUR ÉLECTRONIQUE DE FREINAGE\r\nDIRECTION ASSISTÉE\r\nJANTES ALU\r\nORDINATEUR DE BORD\r\nPEINTURE MÉTAL\r\nPHARES ANTIBROUILLARD\r\nCD/MP3\r\nPRISE USB\r\nCOMMANDE AUDIO AU VOLANT\r\nRÉTROS ÉLECTRIQUES DÉGIVRANTS\r\nRÉTROS RABATTABLES ÉLECTRIQUEMENT\r\nVOLANT CUIR\r\n4 VITRES ÉLECTRIQUES\r\nDÉMARRAGE SANS CLÉ\r\nISOFIX\r\n\r\n- Tous nos véhicules sont révisés et préparés avant livraison\r\n\r\n- Nous achetons tous types de véhicule à partir de 2008, 100.000 km max\r\n\r\nGARAGE DE CHATEL\r\n15 RUE DE VERDUN\r\n57160 CHATEL ST GERMAIN\r\n0674347955\r\n0387305895\r\n\r\nDu Mardi au vendredi 9h-12h / 14h-18h30\r\nSamedi 9h-15h30 ',13000,'Reims','51100','Ayoub El harti','n36@hotmail.fr','759UEMA1dycz2','7500813580','2015-02-03','0664885206','App/template/img/large/7500813580.jpeg',NULL,1),
	(48,'Audi','blabla',13000,'LAXOU','51100','Ayoub El harti','n36@hotmail.fr','81.4eckyZFNAA','8125011086','2015-02-06','0664885206','App/template/img/large/8125011086.jpeg',NULL,1);

/*!40000 ALTER TABLE `annonces` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NomCategorie` varchar(50) NOT NULL,
  `IdDepartement` int(11) NOT NULL,
  `UrlImg` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`Id`, `NomCategorie`, `IdDepartement`, `UrlImg`)
VALUES
	(1,'vehicule',2,'App/template/img/large/9802262167.jpeg'),
	(2,'Immobilier',3,'App/template/img/large/9802262167.jpeg'),
	(3,'multimedia',2,'App/template/img/large/9802262167.jpeg'),
	(4,'MAISON ET JARDIN',2,'App/template/img/large/9802262167.jpeg');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table departements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departements`;

CREATE TABLE `departements` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `NomDepartement` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `departements` WRITE;
/*!40000 ALTER TABLE `departements` DISABLE KEYS */;

INSERT INTO `departements` (`Id`, `NomDepartement`)
VALUES
	(2,'vehicules'),
	(3,'appareil');

/*!40000 ALTER TABLE `departements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table keytoken
# ------------------------------------------------------------

DROP TABLE IF EXISTS `keytoken`;

CREATE TABLE `keytoken` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cle` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `keytoken` WRITE;
/*!40000 ALTER TABLE `keytoken` DISABLE KEYS */;

INSERT INTO `keytoken` (`id`, `cle`)
VALUES
	(1,'ad67retsfa09hyT'),
	(2,'1');

/*!40000 ALTER TABLE `keytoken` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UrlPhoto` varchar(50) NOT NULL,
  `Id_a` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;

INSERT INTO `photos` (`Id`, `UrlPhoto`, `Id_a`)
VALUES
	(1,'App/template/img/501.jpg',1),
	(2,'App/template/img/large/57900837102.jpeg',37),
	(3,'App/template/img/large/62736540542.jpeg',38),
	(4,'App/template/img/large/24612228832.jpeg',39),
	(5,'App/template/img/large/65093715172.jpeg',40),
	(6,'App/template/img/large/75008135802.jpeg',41),
	(7,'App/template/img/large/81250110862.jpeg',48);

/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
