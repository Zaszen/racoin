<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model;
class Departements extends \Illuminate\Database\Eloquent\Model{
    protected $table='departements';
    protected $primaryKey='Id';
    public $timestamps=false;
    
    public function getAnnonce()
    {
        return $this->hasMany('annonces','IdAnnonce');
    }
    public function getCategorie(){
        return $this->hasMany('App\Model\Categories','IdDepartement');
    }
}
