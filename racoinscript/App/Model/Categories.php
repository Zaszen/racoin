<?php

namespace App\Model;
class Categories extends \Illuminate\Database\Eloquent\Model{
    
     protected $table='categories';
     protected $primaryKey='Id';
     public $timestamps=false;
    
    public function annonces() {
        return $this->hasMany('App\Model\Annonces', 'Id_categorie');
    }
    public function departement(){
        return $this->belongsTo('App\Model\Departements','IdDepartement');
    }       
}