<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Keytoken
 *
 * @author Ayoub el harti
 */

namespace App\Model;

class Keytoken extends \Illuminate\Database\Eloquent\Model {

    protected $table = "keytoken";
    protected $primaryKey = 'id';
    public $timestamps = false;

}
