<?php

namespace App\Model;

class Annonces extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'annonces';
    protected $primaryKey = 'Id';
    public $timestamps = false;

    public function categorie() {
        return $this->belongsTo('App\Model\Categories', 'Id_categorie');
    }

    public function photoS() {
        return $this->hasOne('App\Model\Photos', 'Id_a');
    }

    public function crypter($pass, $salt) {
        return crypt($pass, $salt);
    }

}
