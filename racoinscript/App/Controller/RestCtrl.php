<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RestCtrl
 *
 * @author Ayoub EL HARTI
 */

namespace App\Controller;

class RestCtrl {

    //obtenir la liste des annonces en format JSON en faisant un appel REST
    public function restAnnonces($app) {
        $api = $app->request()->get('apikey');
        $limit = $app->request()->get('limit');
        $offset = $app->request()->get('offset');
        $host = $_SERVER['SERVER_NAME'];
        $k = \App\Model\Keytoken::where('cle', '=', $api)->first();
        if (!is_null($k)) {
            $app->response->headers->set('Content-type', 'application/json');
            $app->response()->setStatus(200);
            $an = \App\Model\Annonces::take($limit)->skip($offset)->get();
            if (!is_null($an)) {
                foreach ($an as $n) {
                    $annonce = array("id" => $n->Id, "Titre" => $n->Titre, "Type" => $n->categorie->NomCategorie, "Tarif" => $n->Prix);
                    $url = $host . $app->urlFor("restAnnonceById", array("id" => $n->Id));
                    $links = array("self" => array("href" => $url));
                    $resultat[] = array("annonce" => $annonce, "links" => $links);
                }
                $lim = $limit + 5;
                $off = $offset + 5;
                $urlNext = $host . $app->urlFor("restDetailAnnonces") . "?apikey=" . $api . "&limit=" . $lim . "&offset=" . $off;
                if (($limit - 5 > 0) and ( $offset - 5 > 0)) {
                    $lim = $limit - 5;
                    $off = $offset - 5;
                    $urlPrev = $host . $app->urlFor("restDetailAnnonces") . "?apikey=" . $api . "&limit=" . $lim . "&offset=" . $off;
                } else {
                    $urlPrev = NULL;
                }
                $navigation = array('links' => array(array('next' => $urlNext), array('prev' => $urlPrev)));
                $resultat[] = $navigation;
                $annonces = array("annonces" => $resultat, "status" => "ok");
                echo json_encode($annonces, JSON_PRETTY_PRINT);
            } else {
                $rep = array("userMessage" => "service erreur");
                echo json_encode($rep);
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

//obtenir la liste des catégories en format JSON en faisant un appel REST
    public function restCategories($app) {
        $api = $app->request()->get('apikey');
        $host = $_SERVER['SERVER_NAME'];
        $k = \App\Model\Keytoken::where('cle', '=', $api)->first();
        if (!is_null($k)) {
            $app->response->headers->set('Content-type', 'application/json');
            $app->response()->setStatus(200);
            $cat = \App\Model\Categories::all();
            if (!is_null($cat)) {
                foreach ($cat as $c) {
                    $categorie = array("id" => $c->Id, "libelle" => $c->NomCategorie, "Type" => $c->departement->NomDepartement);
                    $url = $host . $app->urlFor("restCategorieById", array("id" => $c->Id));
                    $links = array("self" => array("href" => $url));
                    $resultat[] = array("categorie" => $categorie, "links" => $links);
                }
                $categories = array("categorie" => $resultat, "status" => "ok");
                echo json_encode($categories, JSON_PRETTY_PRINT);
            } else {
                $rep = array("userMessage" => "service erreur");
                echo json_encode($rep);
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

//obtenir la description détaillé d'une annonce en format JSON en faisant un appel REST
    public function restDetailAnnonces($app) {
        $api = $app->request()->get('apikey');
        $info = $app->request()->get('info');
        $limit = $app->request()->get('limit');
        $offset = $app->request()->get('offset');
        $host = $_SERVER['SERVER_NAME'];
        $k = \App\Model\Keytoken::where('cle', '=', $api)->first();
        if (!is_null($k)) {
            if ($info == 'all') {
                $app->response->headers->set('Content-type', 'application/json');
                $app->response()->setStatus(200);
                $an = \App\Model\Annonces::take($limit)->skip($offset)->get();
                if (!is_null($an)) {
                    foreach ($an as $n) {
                        $url = $host . $app->urlFor("restAnnonceById", array("id" => $n->Id));
                        $categorie = array("id" => $n->categorie->Id, "libelle" => $n->categorie->NomCategorie, "Type" => $n->categorie->departement->NomDepartement);
                        $annonce = array("id" => $n->Id, "Titre" => $n->Titre, "Description" => $n->Description,
                            "Tarif" => $n->Prix, "Ville" => $n->Ville
                            , "Code Postal" => $n->CodeP, 'Nom' => $n->Nom, "Date" => $n->DateP, "Tel" => $n->Tel);
                        $links = array("self" => array("href" => $url), "UrlImage" => array("href" => $host . $app->request()->getRootUri() . '/' . $n->UrlImg));
                        $resultat[] = array("annonce" => $annonce, "categorie" => $categorie, "links" => $links);
                    }
                    $annonces = array("annonces" => $resultat, "status" => "ok");
                    echo json_encode($annonces, JSON_PRETTY_PRINT);
                } else {
                    $rep = array("userMessage" => "service erreur");
                    echo json_encode($rep);
                }
            } else {
                $this->restAnnonces($app);
            }
        } else {
            $rep = array("userMessage" => "paramètres invalide");
            echo json_encode($rep);
        }
    }

    public function restAnnonceDuneCategorie($app, $idCat) {
        $api = $app->request()->get('apikey');
        $k = \App\Model\Keytoken::where('cle', '=', $api)->first();
        if (!is_null($k)) {
            $app->response->headers->set('Content-type', 'application/json');
            $app->response()->setStatus(200);
            $host = $_SERVER['SERVER_NAME'];
            $cat = \App\Model\Categories::find($idCat);
            if (!is_null($cat)) {
                foreach ($cat->annonces as $an) {
                    $annonce = array("type" => $cat->NomCategorie, "id" => $an->Id, 'tarif' => $an->Prix);
                    $url = $host . $app->urlFor("restAnnonceById", array("id" => $an->Id));
                    $links = array("self" => array("href" => $url), "UrlImage" => array("href" => $host . $app->request()->getRootUri() . '/' . $an->UrlImg));
                    $resultat[] = array("annonce" => $annonce, "links" => $links);
                }
                $res = array("annonces" => $resultat, "status" => "ok");
                echo json_encode($res, JSON_PRETTY_PRINT);
            } else {
                echo "aucun resultat trouver";
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

    public function restGetCategorie($id) {
        $c = \App\Model\Categories::find($id);
        if (!is_null($c)) {
            $categorie = array("categorie" => array("id" => $c->Id, "libelle" => $c->NomCategorie, "Type" => $c->departement->NomDepartement));
            echo json_encode($categorie, JSON_PRETTY_PRINT);
        } else {
            $rep = array("userMessage" => "service introuvable");
            echo json_encode($rep);
        }
    }

    //modifier une annonce en faisant un appel REST
    public function restModifierAnnonce($app, $id) {
        $app->response->headers->set('Content-type', 'application/json');
        $app->response()->setStatus(200);
        $apikey = $app->request()->params('apikey');
        $pass = $app->request()->params('pass');
        $k = \App\Model\Keytoken::where('cle', '=', $apikey)->first();
        if (!is_null($k)) {
            $host = $_SERVER['SERVER_NAME'];
            $an = \App\Model\Annonces::find($id);

            if (!is_null($an)) {
                if ($an->Password == $an->crypter($pass, $an->Salt)) {
                    if (!is_null($app->request()->params('titre'))) {
                        $an->Titre = $app->request()->params('titre');
                    }
                    if (!is_null($app->request()->params('description'))) {
                        $an->Description = $app->request()->params('description');
                    }
                    if (!is_null($app->request()->params('tarif'))) {
                        $an->Prix = $app->request()->params('tarif');
                    }
                    if (!is_null($app->request()->params('codep'))) {
                        $an->CodeP = $app->request()->params('codep');
                    }
                    if (!is_null($app->request()->params('nom'))) {
                        $an->Nom = $app->request()->params('nom');
                    }
                    if (!is_null($app->request()->params('tel'))) {
                        $an->Tel = $app->request()->params('tel');
                    }
                    $an->save();
                    $anM = \App\Model\Annonces::find($an->Id);
                    $annonce = array("id" => $anM->Id, "Titre" => $anM->Titre, "Description" => $anM->Description,
                        "Tarif" => $anM->Prix, "Ville" => $anM->Ville
                        , "Code Postal" => $anM->CodeP, 'Nom' => $anM->Nom, "Date" => $anM->DateP, "Tel" => $anM->Tel);
                    $url = $host . $app->request()->getRootUri() . '/' . $app->urlFor('restAnnonceById', array("id" => $an->Id . "?apikey=" . $apikey));
                    $links = array("self" => array("href" => $url));
                    $resultat = array("annonce" => $annonce, "links" => $links);
                    echo json_encode($resultat, JSON_PRETTY_PRINT);
                } else {

                    $rep = array("userMessage" => "Mot de passe incorrect");
                    echo json_encode($rep);
                }
            } else {
                $rep = array("userMessage" => "Annonce Introuvable");
                echo json_encode($rep);
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

    public function getAnnonceJson($id, $app) {
        $apikey = $app->request()->params('apikey');
        $k = \App\Model\Keytoken::where('cle', '=', $apikey)->first();
        if (!is_null($apikey) && !is_null($k)) {
            $host = $_SERVER['SERVER_NAME'];
            $n = \App\Model\Annonces::find($id);
            if (!is_null($n)) {
                $annonce = array("id" => $n->Id, "Titre" => $n->Titre, "Description" => $n->Description,
                    "Tarif" => $n->Prix, "Ville" => $n->Ville
                    , "Code Postal" => $n->CodeP, 'Nom' => $n->Nom, "Date" => $n->DateP, "Tel" => $n->Tel);
                $url = $host . $app->urlFor("restAnnonceById", array("id" => $n->Id));
                $links = array("self" => array("href" => $url), "UrlImage" => array("href" => $host . $app->request()->getRootUri() . '/' . $n->UrlImg));
                $resultat = array("annonce" => $annonce, "links" => $links, "status" => "ok");
                echo json_encode($resultat, JSON_PRETTY_PRINT);
            } else {
                $rep = array("userMessage" => "annonce non trouvable");
                echo json_encode($rep);
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

    //ajouter une annonce en faisant un appel REST
    public function restAddAnnonce($app) {
        $apikey = $app->request()->params('apikey');
        $k = \App\Model\Keytoken::where('cle', '=', $apikey)->first();
        if (!is_null($k)) {
            $titre = $app->request()->params('titre');
            $description = $app->request()->params('description');
            $prix = $app->request()->params('tarif');
            $ville = $app->request()->params('ville');
            $codep = $app->request()->params('codep');
            $nom = $app->request()->params('nom');
            $email = $app->request()->params('email');
            $pass = $app->request()->params('pass');
            $tel = $app->request()->params('tel');
            $date = date('Y-m-d');
            if (is_null($titre) || is_null($description) || is_null($prix) || is_null($ville) || is_null($codep) || is_null($nom) || is_null($email) || is_null($pass) || is_null($tel)) {
                $rep = array("userMessage" => "Veuillez saisir tout les champs");
                echo json_encode($rep);
            } else {
                $a = new \App\Model\Annonces();
                $a->Titre = $titre;
                $a->Description = $description;
                $a->Prix = $prix;
                $a->Ville = $ville;
                $a->CodeP = $codep;
                $a->Nom = $nom;
                $a->Email = $email;
                $gencode = rand(100000, 10000000000);
                $a->Salt = $gencode;
                $a->Password = crypt($pass, $gencode);
                $a->Tel = $tel;
                $a->DateP = $date;
                $a->save();
                $host = $_SERVER['SERVER_NAME'];
                $anM = \App\Model\Annonces::find($a->Id);
                $annonce = array("id" => $anM->Id, "Titre" => $anM->Titre, "Description" => $anM->Description,
                    "Tarif" => $anM->Prix, "Ville" => $anM->Ville
                    , "Code Postal" => $anM->CodeP, 'Nom' => $anM->Nom, "Date" => $anM->DateP, "Tel" => $anM->Tel);
                $url = $host . $app->request()->getRootUri() . '/' . $app->urlFor('restAnnonceById', array("id" => $a->Id . "?apikey=" . $apikey));
                $links = array("self" => array("href" => $url));
                $resultat = array("annonce" => $annonce, "links" => $links, "status" => "ok");
                echo json_encode($resultat, JSON_PRETTY_PRINT);
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

    //obtenir une liste d'annonces répondant à des conditions de filtrage
    public function restFiltre($app) {
        $app->response->headers->set('Content-type', 'application/json');
        $app->response()->setStatus(200);
        $apikey = $app->request()->params('apikey');
        $k = \App\Model\Keytoken::where('cle', '=', $apikey)->first();
        if (!is_null($k)) {
            $titre = $app->request()->params('titre');
            $description = $app->request()->params('description');
            $prixMin = $app->request()->params('min');
            $prixMax = $app->request()->params('max');
            $ville = $app->request()->params('ville');
            if (!is_null($titre) && !is_null($description) && !is_null($prixMin) && !is_null($prixMax) && !is_null($ville)) {
                $an = \App\Model\Annonces::where("Titre", "like", "%" . $titre . "%")
                        ->where("Description", "like", "%" . $description . "%")
                        ->where("Ville", "=", $ville)
                        ->where("Prix", ">=", $prixMin)
                        ->where("Prix", "<=", $prixMax)
                        ->get();
                if ($an->count() > 0) {
                    $host = $_SERVER['SERVER_NAME'];
                    foreach ($an as $a) {
                        $annonce = array("id" => $a->Id, "Titre" => $a->Titre, "Description" => $a->Description,
                            "Tarif" => $a->Prix, "Ville" => $a->Ville
                            , "Code Postal" => $a->CodeP, 'Nom' => $a->Nom, "Date" => $a->DateP, "Tel" => $a->Tel);
                        $url = $host . $app->request()->getRootUri() . '/' . $app->urlFor('restAnnonceById', array("id" => $a->Id . "?apikey=" . $apikey));
                        $links = array("self" => array("href" => $url));
                        $tab[] = array("annonce" => $annonce, "links" => $links);
                    }

                    $resultat = array("annonces" => $tab, "status" => "ok");
                    echo json_encode($resultat, JSON_PRETTY_PRINT);
                } else {
                    $rep = array("userMessage" => "aucun resultat trouver");
                    echo json_encode($rep);
                }
            } else {
                $rep = array("userMessage" => "parametres incorrect");
                echo json_encode($rep);
            }
        } else {
            $rep = array("userMessage" => "ApiKey incorrect");
            echo json_encode($rep);
        }
    }

}
