<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccueilController
 *
 * @author Ayoub
 */

namespace App\Controller;

class AccueilCtrl {

    public function offres($app) {
        $query = $app->request()->get("q");
        $getcat = $app->request()->get("cat");
        $tarifmax = $app->request()->get("max");
        $v = new \App\Views\View("Chercher");
        if (is_numeric($getcat) && is_numeric($tarifmax) && (is_string($query))) {
            $an = \App\Model\Annonces::where("Id_categorie", "=", $getcat)->where("Prix", "<=", $tarifmax)->where("Titre", "like", "%".$query."%")->orWhere("Description", "like", "%".$query."%")->orderBy('DateP', 'DESC')->get();
        } elseif (is_numeric($getcat)) {
            $an = \App\Model\Annonces::where("Id_categorie", "=", $getcat)->orderBy('DateP', 'DESC')->get();
        } elseif (is_numeric($tarifmax)) {
            $an = \App\Model\Annonces::where("Prix", "<=", $tarifmax)->orderBy('DateP', 'DESC')->get();
        } elseif (is_string($query)) {
            $an = \App\Model\Annonces::where("Titre", "like", "%".$query."%")->orWhere("Description", "like", "%".$query."%")->orderBy('DateP', 'DESC')->get();
        } else {
            $an = \App\Model\Annonces::orderBy('DateP', 'DESC')->get();
        }
        $cat = \App\Model\Categories::all();
        if ($an->count() > 0) {
            $v->addVar("tarif", $tarifmax);
            foreach ($an as $a) {
                $url = $app->urlFor('annonce', array('id' => $a->Id));
                $annonces[] = array('annonce' => $a, 'categorie' => $a->categorie, 'url' => $url);
            }
            $v->addVar("annonces", $annonces);
            $v->addVar("UrlIndex", $app->urlFor("chercher"));
            $v->addVar("comboCategorie", $cat);
            echo $v->render();
        } else {
            $v->addVar("UrlIndex", $app->urlFor("chercher"));
            $v->addVar("comboCategorie", $cat);
            echo $v->render();
        }
    }
    
    public function index($app){
        $c=  \App\Model\Categories::all();
        $v = new \App\Views\View("Accueil");
        $v->addVar("UrlIndex", $app->urlFor("chercher"));
        $v->addVar('categorie', $c);
         echo $v->render();
    }

}
