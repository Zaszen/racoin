<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeposerCtrl
 *
 * @author Ayoub
 */

namespace App\Controller;

class DeposerCtrl {

    public function addAnnonce() {
        $app = \Slim\Slim::getInstance();
        $a = new \App\Model\Annonces();
        $a->Id_Categorie = $app->request()->post("CatId");
        $a->Titre = $app->request()->post("Titre");
        $a->Description = $app->request()->post("Description");
        $a->Ville = $app->request()->post("Ville");
        $a->Prix = $app->request()->post("Prix");
        $a->CodeP = $app->request()->post("CodeP");
        $a->Nom = $app->request()->post("Nom");
        $a->Email = $app->request()->post("Email");
        $gencode = rand(100000, 10000000000);
        $a->Salt = $gencode;
        $pass = crypt($app->request()->post("pass"), $gencode);
        $a->Password = $pass;
        $a->Tel = $app->request()->post("Tel");
        $a->DateP = date('Y-m-d');
        $urlimage = "App/template/img/large/" . $gencode . ".jpeg";
        
        $img = \Gregwar\Image\Image::open($_FILES["image"]["tmp_name"][0]);
        $img->forceResize(400, 400);
        $img->jpeg();
        $img->save($urlimage);
        $a->UrlImg = $urlimage;
        $a->save();
        
        $img2 = \Gregwar\Image\Image::open($_FILES["image"]["tmp_name"][1]);
        $img2->forceResize(400, 400);
        $img2->jpeg();
        $img2->save("App/template/img/large/" . $gencode."2".".jpeg");
        $p=new \App\Model\Photos();
        $p->id_a=$a->Id;
        $p->UrlPhoto="App/template/img/large/" . $gencode."2".".jpeg";
        $p->save();
        
        $c = \App\Model\Categories::all();
        $v = new \App\Views\View("AddAnnonce");
        $url = $app->urlFor("addAnnonce");
        $v->addVar("UrlForm", $url);
        $v->addVar("rep", "1");
        $v->addVar("Categories", $c);
        echo $v->render();
    }

    public function Annonce() {
        $c = \App\Model\Categories::all();
        $v = new \App\Views\View("AddAnnonce");
        $v->addVar("Categories", $c);
        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor("addAnnonce");
        $v->addVar("UrlForm", $url);
        echo $v->render();
    }

}
