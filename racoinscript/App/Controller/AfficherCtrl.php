<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AfficherCtrl 
 *
 * @author Ayoub
 */

namespace App\Controller;

class AfficherCtrl{

    public function getAnnonce($id) {
        $annonce = \App\Model\Annonces::with("categorie","photoS")->find($id);
        if (isset($annonce)) {
            $v = new \App\Views\View("DescAnnonce");
            $v->addVar('annonce', $annonce);
            echo $v->render();
        } else {
            $v = new \App\Views\View("Erreur");
            $v->addVar('message', "Oops , Erreur 404");
            echo $v->render();
        }
    }

}
