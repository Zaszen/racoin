<?php

/* By :EL HARTI Ayoub */

namespace App\Views;

class View {

    private $layout = null;
    private $arrayVar;

    public function __construct($layout) {
        $this->layout = $layout . ".twig";
    }

    public function addVar($var, $val) {
        $this->arrayVar[$var] = $val;
    }

    public function render() {
        $app = \Slim\Slim::getInstance();
        $urlBase = $app->request->getRootUri();
        $this->arrayVar['urlBase'] = $urlBase;
        $chercher = $app->urlFor('chercher');
        $gerer = $app->urlFor('gerer');
        $deposer = $app->urlFor('deposer');
        $index = $app->urlFor('index');
        $recommendations = \App\Model\Annonces::orderByRaw("RAND()")->take(3)->get();
        foreach ($recommendations as $r) {
            $url = $app->urlFor('annonce', array('id' => $r->Id));
            $rec[] = array('roc' => $r, 'url' => $url);
        }
        $this->arrayVar['recommendations'] = $rec;
        $this->arrayVar['menu'] = array('chercher' => $chercher, 'gerer' => $gerer, 'deposer' => $deposer, 'index' => $index);
        $loader = new \Twig_Loader_Filesystem('App/template/twig');
        $twig = new \Twig_Environment($loader);
        $tmpl = $twig->loadTemplate($this->layout);
        return $tmpl->render($this->arrayVar);
    }

    public function display() {
        echo $this->render();
    }

}
