<?php

require_once 'vendor/autoload.php';
$app = new Slim\Slim();

$app->get('/', function() use($app) {
    $c = new \App\Controller\AccueilCtrl();
    $c->index($app);
})->name('index');

$app->get('/:id', function($id) {
    $c = new \App\Controller\AfficherCtrl();
    $c->getAnnonce($id);
})->name('annonce');

$app->get("/offres/", function() use($app) {
    $c = new \App\Controller\AccueilCtrl();
    $c->offres($app);
})->name("chercher");

$app->get('/deposer/', function () {
    $c = new App\Controller\DeposerCtrl();
    $c->Annonce();
})->name('deposer');

$app->post('/deposer/add/', function () {
    $c = new App\Controller\DeposerCtrl();
    $c->addAnnonce();
})->name('addAnnonce');

$app->get('/gerer/', function () {
    echo 'gerer';
})->name('gerer');


$app->group('/rest/annonces', function() use($app) {

    $app->get("/:id", function($id) use($app) {
        $c = new \App\Controller\RestCtrl();
        $c->getAnnonceJson($id, $app);
    })->name("restAnnonceById");

    //obtenir la description détaillé d'une annonce en format JSON en faisant un appel REST
    $app->get("/", function()use($app) {
        $c = new \App\Controller\RestCtrl();
        $c->restDetailAnnonces($app);
    })->name('restDetailAnnonces');

    //obtenir une liste d'annonces répondant à des conditions de filtrage
    $app->get("/search/q", function()use($app) {
        $c=new \App\Controller\RestCtrl();
        $c->restFiltre($app);
    });

    //ajouter une annonce en faisant un appel REST
    $app->put("/add", function() use($app) {
        $c = new App\Controller\RestCtrl();
        $c->restAddAnnonce($app);
    });

    //modifier une annonce en faisant un appel REST
    $app->put("/:id/", function($id) use($app) {
        $c = new App\Controller\RestCtrl();
        $c->restModifierAnnonce($app, $id);
    });
});

//obtenir la liste des catégories en format JSON en faisant un appel REST

$app->group("/rest/categories", function ()use($app) {
    $app->get("/", function()use($app) {
        $c = new \App\Controller\RestCtrl();
        $c->restCategories($app);
    })->name('restCategories');

    $app->get("/:id", function($id) {
        $c = new \App\Controller\RestCtrl();
        $c->restGetCategorie($id);
    })->name("restCategorieById");
    
//obtenir la liste des annonces appartenant à une catégorie
    $app->get('/:id/annonces/', function($id) use($app) {
        $c = new App\Controller\RestCtrl();
        $c->restAnnonceDuneCategorie($app, $id);
    });
});

$app->run();
